# 一个好看的消息提示插件 基于Vue


## 在线演示
[https://tczmh.gitee.io/alert-message-vue/](https://tczmh.gitee.io/alert-message-vue/)

## 效果
![效果演示](https://images.gitee.com/uploads/images/2019/0215/123610_53f424ac_2068998.png "微信截图_20190215123547.png")

## 申明
代码并非纯原创，是基于这个jQuery例子，只是将其改写成了Vue，原代码出处是下面的链接
[jquery消息提示插件：类vue element message组件功能](http://www.jq22.com/jquery-info16308)

## 用法
```javascript
Alert('操作成功 啦啦啦','success',2000)
```